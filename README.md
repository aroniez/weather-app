# WeatherApp

Introduction:
=====
An open-source Weather app that consumes data from https://openweathermap.org/

Build Instructions:
=====

1. Clone the Project

2. Gradle Sync and Rebuild

Dependencies:
=====

### [Kotlin](https://kotlinlang.org/):

The app is written entirely in kotlin

### [JetPack](https://developer.android.com/jetpack) - Architecture Components & AndroidX:
<br>**Room** - Database Layer
<br>**ViewModel** - Data Preservation across configuration changes
<br>**Lifecycle** - Handling annoying issues with Activities / Fragments namely when pushing data during false states
<br>**Navigation** - Handling Intent / Fragment Transactions, isolating sources from destinations and easy argument passing!
<br>**AndroidX, Material Components** - For embracing Material Design and backporting API features to minSdk

### [JUnit4](https://junit.org/junit4/) + [Mockito](https://site.mockito.org/) - Unit Testing:
Combination for unit testing and mocking dependencies

# Contributions:

Contributions in any form are welcome.