package com.aroniez.weather

import androidx.room.Room
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.aroniez.weather.data.WeatherDatabase
import com.aroniez.weather.data.daos.WeatherDao
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class ReportsDaoTest {
    private lateinit var weatherDatabase: WeatherDatabase
    private lateinit var weatherDao: WeatherDao


    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        weatherDatabase = Room.inMemoryDatabaseBuilder(context, WeatherDatabase::class.java).build()
        weatherDao = weatherDatabase.weatherDao()
    }

    @After
    fun closeDb() {
        weatherDatabase.close()
    }

    @Test
    fun when_Database_is_Empty_return_zero_data() {
        // When
        val count = weatherDao.forecastCount()

        // Then
        Truth.assertThat(count).isEqualTo(0)
    }

}