package com.aroniez.weather.configs

const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
const val DATABASE_NAME = "weather-db"
const val UNITS_OF_MEASUREMENT = "metric"
const val DEFAULT_CITY = "New York"