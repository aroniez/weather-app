package com.aroniez.weather.data.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.aroniez.weather.data.dto.weather.WeatherCallback

/**
 * The Data Access Object for the [WeatherCallback] class.
 */
@Dao
interface WeatherDao : BaseDao<WeatherCallback> {
    @Query("SELECT * FROM weather WHERE name =:cityName")
    fun locationWeather(cityName: String): LiveData<WeatherCallback>

    @Query("SELECT * FROM weather WHERE name =:cityName LIMIT 5")
    fun locationForecast(cityName: String): LiveData<List<WeatherCallback>>

    @Query("SELECT COUNT(*) FROM weather")
    fun forecastCount(): Int

    @Query("DELETE FROM weather")
    fun deleteAll()
}