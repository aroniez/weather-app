package com.aroniez.weather.data.dto.weather

import java.io.Serializable

class Coordinate : Serializable {
    var lon: Double = 0.0
    var lat: Double = 0.0
}