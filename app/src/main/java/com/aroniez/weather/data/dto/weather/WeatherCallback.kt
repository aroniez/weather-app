package com.aroniez.weather.data.dto.weather

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "weather")
class WeatherCallback : Serializable {
    var coord: Coordinate? = null
    var weather: List<Weather>? = arrayListOf()
    var main: MainWeather? = null
    var visibility: Int = 0
    var wind: Wind? = null
    var clouds: Cloud? = null
    var sys: SystemWeather? = null
    @PrimaryKey
    var dt: Long = 0L
    var timezone: Int = 0
    var name: String? = ""
    @ColumnInfo(name="date")
    var dt_txt: String? = ""
    var cod: Int? = 0
}