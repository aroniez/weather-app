package com.aroniez.weather.data.dto

import androidx.room.Entity
import java.io.Serializable

class Location : Serializable {
    var place_id: String = ""
    var place_name: String = ""
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var favorite: Boolean = false
}