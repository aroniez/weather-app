package com.aroniez.weather.data.dto.converters

import androidx.room.TypeConverter
import com.aroniez.weather.data.dto.weather.Coordinate
import com.aroniez.weather.data.dto.weather.Wind
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class WindConverter {

    @TypeConverter
    fun fromStringToWind(value: String): Wind {
        val listType = object : TypeToken<Wind>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromWindToString(wind: Wind): String {
        return Gson().toJson(wind)
    }

}
