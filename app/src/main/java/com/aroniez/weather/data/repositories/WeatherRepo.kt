package com.aroniez.weather.data.repositories

import androidx.lifecycle.LiveData
import com.aroniez.weather.AppExecutors
import com.aroniez.weather.api.NetworkBoundResource
import com.aroniez.weather.api.Resource
import com.aroniez.weather.api.callbacks.ForecastCallback
import com.aroniez.weather.api.endpoints.WeatherEndpoints
import com.aroniez.weather.configs.UNITS_OF_MEASUREMENT
import com.aroniez.weather.data.daos.WeatherDao
import com.aroniez.weather.data.dto.weather.WeatherCallback
import com.aroniez.weather.utilities.InjectorUtils

class WeatherRepo private constructor(
        private val weatherDao: WeatherDao,
        private val appExecutors: AppExecutors
) {

    fun weather(cityName: String): LiveData<Resource<WeatherCallback>> {
        return object : NetworkBoundResource<WeatherCallback, WeatherCallback>(appExecutors) {
            override fun saveCallResult(item: WeatherCallback) {
                weatherDao.insert(item)
            }

            override fun shouldFetch(data: WeatherCallback?) = true

            override fun loadFromDb() = weatherDao.locationWeather(cityName)

            override fun createCall() =
                    InjectorUtils.provideRetrofitAdapter(WeatherEndpoints::class.java).todayForecast(cityName, UNITS_OF_MEASUREMENT)
        }.asLiveData()
    }

    fun forecast(cityName: String): LiveData<Resource<List<WeatherCallback>>> {
        return object : NetworkBoundResource<List<WeatherCallback>, ForecastCallback>(appExecutors) {
            override fun saveCallResult(item: ForecastCallback) {
                val updatedList : ArrayList<WeatherCallback> = arrayListOf()
                for(forecast in item.list){
                    forecast.name = cityName
                    updatedList.add(forecast)
                }
                weatherDao.insertAll(updatedList.take(5))
            }

            override fun shouldFetch(data: List<WeatherCallback>?) = true

            override fun loadFromDb() = weatherDao.locationForecast(cityName)

            override fun createCall() =
                    InjectorUtils.provideRetrofitAdapter(WeatherEndpoints::class.java).fiveDayForecast(cityName, UNITS_OF_MEASUREMENT)
        }.asLiveData()
    }


    companion object {

        @Volatile
        private var instance: WeatherRepo? = null

        fun getInstance(weatherDao: WeatherDao, appExecutors: AppExecutors) =
                instance ?: synchronized(this) {
                    instance
                            ?: WeatherRepo(weatherDao, appExecutors).also { instance = it }
                }
    }
}