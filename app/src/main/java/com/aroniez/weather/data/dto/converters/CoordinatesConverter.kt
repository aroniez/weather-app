package com.aroniez.weather.data.dto.converters

import androidx.room.TypeConverter
import com.aroniez.weather.data.dto.weather.Coordinate
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CoordinatesConverter {

    @TypeConverter
    fun fromStringToCoordinate(value: String?): Coordinate? {
        val listType = object : TypeToken<Coordinate>() {}.type
        return if(value == null) null else Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromCoordinateToString(dto: Coordinate?): String {
        return Gson().toJson(dto)
    }

}
