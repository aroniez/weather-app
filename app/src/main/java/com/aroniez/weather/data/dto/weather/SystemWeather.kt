package com.aroniez.weather.data.dto.weather

import java.io.Serializable

class SystemWeather : Serializable {
    var type: Int = 0
    var id: Long = 0L
    var sunrise: Long = 0L
    var sunset: Long = 0L
}