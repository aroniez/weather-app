package com.aroniez.weather.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aroniez.weather.configs.DATABASE_NAME
import com.aroniez.weather.data.daos.WeatherDao
import com.aroniez.weather.data.dto.converters.*
import com.aroniez.weather.data.dto.weather.WeatherCallback


@Database(entities = [WeatherCallback::class], version = 1, exportSchema = false)
@TypeConverters(
        CloudConverter::class,
        CoordinatesConverter::class,
        MainWeatherConverter::class,
        SystemWeatherConverter::class,
        WeatherConverter::class,
        WindConverter::class,
)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao

    companion object {

        @Volatile
        private var instance: WeatherDatabase? = null

        fun getInstance(context: Context): WeatherDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): WeatherDatabase {
            return Room.databaseBuilder(context, WeatherDatabase::class.java, DATABASE_NAME)
                    .addCallback(object : RoomDatabase.Callback() {})
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}
