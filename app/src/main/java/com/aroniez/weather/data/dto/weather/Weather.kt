package com.aroniez.weather.data.dto.weather

import java.io.Serializable

class Weather : Serializable {
    var id: Long = 0L
    var main: String = ""
    var description: String = ""
    var icon: String = ""
}