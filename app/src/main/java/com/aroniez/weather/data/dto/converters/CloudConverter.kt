package com.aroniez.weather.data.dto.converters

import androidx.room.TypeConverter
import com.aroniez.weather.data.dto.weather.Cloud
import com.aroniez.weather.data.dto.weather.Coordinate
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CloudConverter {

    @TypeConverter
    fun fromStringToCloud(value: String): Cloud {
        val listType = object : TypeToken<Cloud>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromCloudToString(cloud: Cloud): String {
        return Gson().toJson(cloud)
    }

}
