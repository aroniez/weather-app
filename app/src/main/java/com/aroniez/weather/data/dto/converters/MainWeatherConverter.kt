package com.aroniez.weather.data.dto.converters

import androidx.room.TypeConverter
import com.aroniez.weather.data.dto.weather.MainWeather
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MainWeatherConverter {

    @TypeConverter
    fun fromStringToMainWeather(value: String): MainWeather {
        val listType = object : TypeToken<MainWeather>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromMainWeatherToString(mainWeather: MainWeather): String {
        return Gson().toJson(mainWeather)
    }

}
