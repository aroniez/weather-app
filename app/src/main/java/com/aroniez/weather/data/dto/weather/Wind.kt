package com.aroniez.weather.data.dto.weather

import java.io.Serializable

class Wind : Serializable {
    var speed: Double = 0.0
    var deg: Int = 0
    var gust: Double = 0.0
}