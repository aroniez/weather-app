package com.aroniez.weather.data.dto.converters

import androidx.room.TypeConverter
import com.aroniez.weather.data.dto.weather.Coordinate
import com.aroniez.weather.data.dto.weather.SystemWeather
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SystemWeatherConverter {

    @TypeConverter
    fun fromStringToSystemWeather(value: String): SystemWeather {
        val listType = object : TypeToken<SystemWeather>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromSystemWeatherToString(systemWeather: SystemWeather): String {
        return Gson().toJson(systemWeather)
    }

}
