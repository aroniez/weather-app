package com.aroniez.weather.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.aroniez.weather.data.repositories.WeatherRepo

class WeatherViewModel(private val weatherRepo: WeatherRepo) : ViewModel() {

    fun getLocationWeather(location: String) = weatherRepo.weather(location)

    fun getForecastWeather(location: String) = weatherRepo.forecast(location)
}