package com.aroniez.weather.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aroniez.weather.R
import com.aroniez.weather.data.dto.weather.WeatherCallback
import com.aroniez.weather.utilities.getDayOfTheWeek
import com.aroniez.weather.utilities.getTimeFromDateTime
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.forecast_row_item.view.*

class ForecastAdapter(private val locations: List<WeatherCallback>) : RecyclerView.Adapter<ForecastAdapter.LocationViewHolder>() {
    class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(weatherCallback: WeatherCallback) {
            itemView.textViewTimeOfDay.text = getTimeFromDateTime(weatherCallback.dt_txt!!)
            itemView.textViewDayOfWeek.text = getDayOfTheWeek(weatherCallback.dt)
            itemView.textViewTemp.text = "${weatherCallback.main?.temp?.toInt()}°"
            itemView.minTemp.text = "${weatherCallback.main?.temp_min?.toInt()}°"
            itemView.maxTemp.text = "${weatherCallback.main?.temp_max?.toInt()}°"
            Picasso.get()
                    .load("http://openweathermap.org/img/w/${weatherCallback.weather?.get(0)?.icon}.png")
                    .into(itemView.imageViewForecastIcon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        return LocationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.forecast_row_item, parent, false))
    }

    override fun getItemCount() = locations.size

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        holder.bindData(locations[position])
    }
}