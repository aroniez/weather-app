package com.aroniez.weather.ui.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aroniez.weather.R
import com.aroniez.weather.api.Status
import com.aroniez.weather.ui.viewmodel.WeatherViewModel
import com.aroniez.weather.utilities.InjectorUtils
import com.aroniez.weather.utilities.getRecentCity
import com.aroniez.weather.utilities.saveRecentCity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_city_screen.*

class ForecastFragment : Fragment(R.layout.fragment_city_screen) {
    private val viewModel: WeatherViewModel by viewModels {
        InjectorUtils.provideWeatherRepoFactory(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeViews()
        searchLocation.setOnClickListener {
            val searchQuery = searchBox.text.toString()
            if (searchQuery.isEmpty() || searchQuery.length < 3) {
                Toast.makeText(requireContext(), "Search query is too short :(", Toast.LENGTH_LONG).show()
            } else {
                getTodayWeather(searchQuery)
                getFiveDayForecast(searchQuery)
            }

        }
    }

    private fun initializeViews() {
        val recentlySearchedCity = getRecentCity(requireContext())
        searchBox.setText(recentlySearchedCity)
        getTodayWeather(recentlySearchedCity)
        getFiveDayForecast(recentlySearchedCity)
    }

    private fun getTodayWeather(location: String) {
        viewModel
                .getLocationWeather(location)
                .observe(viewLifecycleOwner, { weather ->
                    when (weather.status.name) {
                        Status.LOADING.name -> showLoadingProgress("Checking weather for $location")
                        Status.ERROR.name -> showMessageLayout("Something went wrong")
                        else -> {
                            if (weather.data != null) {
                                saveRecentCity(requireContext(), location)
                                showWeatherLayout()
                                textViewTemperature.text = "${weather.data.main!!.temp}°"
                                textViewWeatherMain.text = weather.data.weather!![0].description
                                textViewHumidity.text = weather.data.main!!.humidity.toString()

                                Picasso.get()
                                        .load("http://openweathermap.org/img/w/${weather.data.weather!![0].icon}.png")
                                        .resize(124, 124)
                                        .into(imageViewWeatherIcon)
                            } else {
                                showMessageLayout("Search result for '$location' not found")
                            }
                        }
                    }
                })
    }

    private fun getFiveDayForecast(location: String) {
        viewModel
                .getForecastWeather(location)
                .observe(viewLifecycleOwner, { weather ->
                    when (weather.status.name) {
                        Status.LOADING.name -> showLoadingProgress("Checking weather for $location")
                        Status.ERROR.name -> showMessageLayout("Something went wrong")
                        else -> {
                            if (weather.data != null) {
                                if (weather.data.isNotEmpty()) {
                                    showWeatherLayout()
                                    recyclerForecast.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                                    recyclerForecast.adapter = ForecastAdapter(weather.data)
                                } else {
                                    showMessageLayout("Weather forecast for $location not found")
                                }
                            }
                        }
                    }
                })
    }

    private fun showLoadingProgress(message: String) {
        loading_layout.visibility = View.VISIBLE
        mainView.visibility = View.GONE
        messageTv.text = message
    }

    private fun showMessageLayout(message: String) {
        loading_layout.visibility = View.VISIBLE
        progressBar1.visibility = View.GONE
        mainView.visibility = View.GONE
        messageTv.text = message
    }

    private fun showWeatherLayout() {
        loading_layout.visibility = View.GONE
        mainView.visibility = View.VISIBLE
    }
}