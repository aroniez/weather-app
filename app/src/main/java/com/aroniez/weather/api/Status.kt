package com.aroniez.weather.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
