package com.aroniez.weather.api.endpoints

import androidx.lifecycle.LiveData
import com.aroniez.weather.api.ApiResponse
import com.aroniez.weather.api.callbacks.ForecastCallback
import com.aroniez.weather.data.dto.weather.WeatherCallback
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherEndpoints {

    @GET("weather?&appid=c6e381d8c7ff98f0fee43775817cf6ad")
    fun todayForecast(
            @Query("q") cityName: String,
            @Query("units") units: String
    ): LiveData<ApiResponse<WeatherCallback>>

    @GET("forecast?&appid=c6e381d8c7ff98f0fee43775817cf6ad")
    fun fiveDayForecast(
            @Query("q") cityName: String,
            @Query("units") units: String
    ): LiveData<ApiResponse<ForecastCallback>>

}
