package com.aroniez.weather.api.callbacks

import com.aroniez.weather.data.dto.weather.WeatherCallback

class ForecastCallback(
        var message: Int,
        var cnt: Int,
        var cod: String,
        var list: List<WeatherCallback>
)