package com.aroniez.weather.utilities

import java.text.SimpleDateFormat
import java.util.*

fun getDayOfTheWeek(date: Long): String {
    val outFormat = SimpleDateFormat("EEEE", Locale.getDefault())
    return outFormat.format(Date(date))
}

fun getTimeFromDateTime(date: String): String {
    return if (date.length < 15) {
        ""
    } else {
        date.substring(11, 16)
    }
}