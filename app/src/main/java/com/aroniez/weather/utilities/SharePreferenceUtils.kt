package com.aroniez.weather.utilities

import android.content.Context
import com.aroniez.weather.configs.DEFAULT_CITY

private const val userPrefName = "user"
private const val recentlySearchedCity = "recentlySearchedCity"

private fun userPrefs(c: Context) = c.getSharedPreferences(userPrefName, Context.MODE_PRIVATE)

fun getRecentCity(context: Context) = userPrefs(context).getString(recentlySearchedCity, DEFAULT_CITY)!!
fun saveRecentCity(c: Context, token: String) = userPrefs(c).edit().putString(recentlySearchedCity, token).apply()