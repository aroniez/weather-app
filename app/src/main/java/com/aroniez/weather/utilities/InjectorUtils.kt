package com.aroniez.weather.utilities

import android.content.Context
import android.util.Log
import com.aroniez.weather.AppExecutors
import com.aroniez.weather.BuildConfig
import com.aroniez.weather.api.LiveDataCallAdapterFactory
import com.aroniez.weather.configs.BASE_URL
import com.aroniez.weather.data.WeatherDatabase
import com.aroniez.weather.data.repositories.WeatherRepo
import com.aroniez.weather.ui.viewmodel.WeatherViewModelFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object InjectorUtils {

    private fun weatherRepo(c: Context) = WeatherRepo.getInstance(WeatherDatabase.getInstance(c).weatherDao(), AppExecutors())
    fun provideWeatherRepoFactory(context: Context) = WeatherViewModelFactory(weatherRepo(context))

    private fun provideOkHttpClient(): OkHttpClient {
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Log.d("API", it)
        })
        if (BuildConfig.DEBUG) {
            logger.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logger.level = HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
                .addInterceptor(logger)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build()
    }

    fun <I> provideRetrofitAdapter(serviceClass: Class<I>): I {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
                .create(serviceClass)
    }
}
