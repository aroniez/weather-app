package com.aroniez.weather

import com.aroniez.weather.utilities.getTimeFromDateTime
import org.junit.Assert.assertEquals
import org.junit.Test

class PhoneNumberMaskerTest {

    @Test
    fun `given invalid date, return empty time`() {
        assertEquals(getTimeFromDateTime(""), "")
    }

    @Test
    fun `given a valid date, return correct time`() {
        assertEquals(getTimeFromDateTime("2021-05-01 09:00:00"), "09:00")
    }
}